### $`\textcolor{GoldenRod}{\text{響應式網頁的初始化}}`$
```
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="new_bootstrap/dist/css/bootstrap.min.css">
  <title>新商業版型 A</title>

  <style>
    .carousel-item img
    {
      height: 300px;
    }

    .carousel-caption
    {
      margin-bottom: 50px;
    }

    .carousel-caption *
    {
      color: GreenYellow;
      font-weight: bold;
      text-align: center;
    }

    .carousel-indicators button
    {

      height: 20px !important;
    }
  </style>
</head>
```
由上可看出，此網頁的編碼方式 (utf-8)、預設尺寸 (initial-scale=1)、所套用的串接樣式表檔案 (new_bootstrap/dist/css/bootstrap.min.css)，以及影像輪播器的相關外觀設定 (「.carousel」開頭之相關選取器語法)。


### $`\textcolor{GoldenRod}{\text{網頁中之導覽列的相關語法}}`$
```
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">
      <img src="images/花朵相關/flower-icon.png" style="width: 30px ; position: relative ; top: -5px">
    </a>
     ⋮
  </div>
</nav>
```

### $`\textcolor{GoldenRod}{\text{網頁中之影像輪播器的相關語法}}`$
```
<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-indicators">
   ⋮
  </div>
  <div class="carousel-inner">
   ⋮
  </div>
 ⋮
</div>
```

### $`\textcolor{GoldenRod}{\text{其他響應式區塊的相關語法}}`$
```
<div class="container text-center">
  <h2>賞花嚮宴</h2>
  <div class="row">
    <div class="col-md-4">
     ⋮
    </div>
    <div class="col-md-4">
     ⋮
    </div>

    <div class="col-md-4">
     ⋮
    </div>
  </div>
</div>
```

### $`\textcolor{GoldenRod}{\text{頁尾的相關語法}}`$
```
<footer class="container-fluid text-center">
  <p>花雀蘭奇股份有限公司 &copy;　202X</p>
</footer>
```

### $`\textcolor{GoldenRod}{\text{繫結 Bootstrap 響應式機制所需的 JavaScript 程式碼檔案 bootstrap.bundle.min.js。}}`$
```
<script src="new_bootstrap/dist/js/bootstrap.bundle.min.js"></script>
```

### $`\textcolor{GoldenRod}{\text{該作品的參考網址}}`$

[響應式網頁 之 簡易作品 A](https://francesco-artworks.web.app/responsive/layout-a-new.html)
